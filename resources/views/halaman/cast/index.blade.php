@extends('layout.master')

@section('header', 'Daftar Cast')

@section('content')
<a href="/cast/create" class="btn btn-success btn-sm mb-2">Tambah Data</a>
<table class="table table-bordered">
	<thead>                  
		<tr>
			<th style="width: 5%">#</th>
			<th>Nama</th>
			<th style="width: 10%">Umur</th>
			{{-- <th>Bio</th> --}}
			<th style="width: 20%">Aksi</th>
		</tr>
	</thead>
	<tbody>
		@forelse ($casts as $key => $cast)
			<tr>
				<td>{{ $key+1 }}</td>
				<td>{{ $cast->nama }}</td>
				<td>{{ $cast->umur }}</td>
				{{-- <td>{{ $cast->bio }}</td> --}}
				<td>
					<a href="/cast/{{ $cast->id }}" class="btn btn-sm btn-info">Show</a>
					<a href="/cast/{{ $cast->id }}/edit" class="btn btn-sm btn-primary">Edit</a>
					<form onclick="return confirm('hapus data?')" action="/cast/{{$cast->id}}" method="POST" class="d-inline">
						@method('DELETE')
						@csrf
						<button type="submit" class="btn btn-danger btn-sm">Delete</button>
					</form>
				</td>
			</tr>
		@empty
			<tr>
				<td colspan="4" class="text-center">Data Kosong</td>
			</tr>
		@endforelse
	</tbody>
</table>
@endsection