@extends('layout.master')

@section('header', 'Detail Cast')

@section('content')

<p>Nama : {{ $cast->nama }}</p>
<p>Umur : {{ $cast->umur }}</p>
<p>Bio : {{ $cast->bio }}</p>
<a href="/cast" class="mt-3">Kembali</a>

@endsection