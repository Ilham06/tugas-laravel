@extends('layout.master')

@section('header', 'Tambah Data Cast')

@section('content')
<form action="/cast" method="post">
	@csrf
	<div class="form-group">
		<label>Nama</label>
		<input type="text" class="form-control" name="nama" placeholder="Masukan Nama">
		@error('nama')
			<small class="text-danger">{{ $message }}</small>
		@enderror
	</div>
	<div class="form-group">
		<label>Umur</label>
		<input type="number" class="form-control" name="umur" placeholder="Masukan umur">
		@error('umur')
			<small class="text-danger">{{ $message }}</small>
		@enderror
	</div>
	<div class="form-group">
		<label>Bio</label>
		<textarea class="form-control" rows="3" name="bio" placeholder="Masukan Bio"></textarea>
		@error('bio')
			<small class="text-danger">{{ $message }}</small>
		@enderror
	</div>
	<button type="submit" class="btn btn-success mt-2">Simpan</button>
</form>
@endsection