@extends('layout.master')

@section('header', 'Register')

@section('content')
	<h1>Buat Account Baru!</h1>
	<h3>Sign Up Form</h3>
	<form action="/welcome" method="post">
		@csrf
		<p>First name:</p>
		<input type="text" id="fname" name="fname"><br>
		<p>Last name:</p>
		<input type="text" id="lname" name="lname">
		<p>Gender:</p>
		<input type="radio" id="male" name="gender" value="male">
		<label for="male">Male</label><br>
		<input type="radio" id="female" name="gender" value="female">
		<label for="female">Female</label><br>
		<input type="radio" id="other" name="gender" value="other">
		<label for="other">Other</label>
		<p>Nationality:</p>
		<select name="" id="">
			<option value="indonesian">Indonesian</option>
			<option value="malaysian">Malaysian</option>
			<option value="singapore">Singapore</option>
		</select>
		<p>Language Spoken:</p>
		<input type="checkbox" id="indonesian" name="indonesian" value="indonesian">
		<label for="indonesian">Indonesian</label><br>
		<input type="checkbox" id="english" name="english" value="english">
		<label for="english">English</label><br>
		<input type="checkbox" id="other" name="other" value="other">
		<label for="other">Other</label>
		<p>Bio:</p>
		<textarea name="" id="" cols="30" rows="10"></textarea>
		<br>
		<button type="submit">Sign Up</button>
	</form>
@endsection