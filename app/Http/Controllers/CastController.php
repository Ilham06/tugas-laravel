<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function index()
    {
        $casts = DB::table('cast')->get();

        return view('halaman.cast.index', [
            'casts' => $casts
        ]);
    }

    public function create()
    {
        return view('halaman.cast.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:cast|max:45',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        $query = DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);

        return redirect('/cast');
    }

    public function show($cast_id)
    {
        $cast = DB::table('cast')->where('id', $cast_id)->first();

        return view('halaman.cast.show', [
            'cast' => $cast
        ]);
    }

    public function edit($cast_id)
    {
        $cast = DB::table('cast')->where('id', $cast_id)->first();

        return view('halaman.cast.edit', [
            'cast' => $cast
        ]);
    }

    public function update($cast_id, Request $request)
    {
        $request->validate([
            'nama' => 'required|max:45',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        $query = DB::table('cast')
            ->where('id', $cast_id)
            ->update([
                'nama' => $request["nama"],
                'umur' => $request["umur"],
                'bio' => $request["bio"]
            ]);

        return redirect('/cast');
    }

    public function destroy($cast_id)
    {
        $query = DB::table('cast')->where('id', $cast_id)->delete();

        return redirect('/cast');
    }
}
