<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TableController extends Controller
{
    public function table()
    {
        return view('halaman.table.table');
    }

    public function dataTable()
    {
        return view('halaman.table.datatable');
    }
}
